// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "BetrayalGameMode.h"
#include "BetrayalHUD.h"
#include "BetrayalCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABetrayalGameMode::ABetrayalGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABetrayalHUD::StaticClass();
}
